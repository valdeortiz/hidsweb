from flask import Flask, render_template, request, session, redirect,url_for,flash
from flask_login import current_user, LoginManager, login_required, login_user, logout_user

#from flask_mysqldb import MySQL
import bcrypt

from forms import LoginForm

from models import UserData, UserModel, get_user

from flask_bootstrap import Bootstrap


semilla = bcrypt.gensalt()

app = Flask(__name__)
bootstrap = Bootstrap(app)

app.secret_key = "secret_key"
"""
mysql = MySQL(app)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'dbHids'
"""
login_manager = LoginManager()
login_manager.login_view = '/login'
login_manager.init_app(app)

BASE_URL = "/home/valdeortiz/Desktop/archivos"

@login_manager.user_loader
def load_user(user_id):
    return UserModel.query(user_id)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data

        user_doc = get_user(username)

        if user_doc is not None:
            print("hola " + user_doc.password)
            password_from_db = user_doc.password
            if password == password_from_db:
                user_data = UserData(username, password)
                user = UserModel(user_data)
                login_user(user)
                flash('Bienvenido')
                return redirect(url_for('home'))
            else:
                flash('La informacion no coincide')
        else:
                flash('El usuario no existe')

    return render_template('login.html', login_form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('Regresa pronto')
    return redirect(url_for('login'))


@app.route('/')
@login_required
def home():
    return render_template('home.html', username=current_user.id)

@app.route('/about')
@login_required
def about():
    return render_template('about.html')

@app.route('/<id>')
def lista_resultado(id):
    # aca tiene que ejecutar el .py del hids para que cree el archivo
    # os.sytem(...)
    title = request.args.get('titulo')
    with open(f'{BASE_URL}/{id}.txt', 'r') as f:
        data = f.read()
        context = {
        'titulo': title, #f'Lista {id}',
        'data' : data.split('.'),
        }
        #print(data)

    return render_template('data_card.html', **context)

if __name__ == '__main__':
    app.run()

